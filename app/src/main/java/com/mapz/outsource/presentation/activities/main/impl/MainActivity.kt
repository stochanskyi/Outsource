package com.mapz.outsource.presentation.activities.main.impl

import com.mapz.outsource.presentation.activities.main.MainContract
import com.mapz.outsource.presentation.mvp.BaseActivity
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity<MainContract.PresenterContract>(),
    MainContract.ViewContract {
    override val presenter: MainContract.PresenterContract by inject()

    override fun onInitPresenter() {
        presenter.view = this
    }


}