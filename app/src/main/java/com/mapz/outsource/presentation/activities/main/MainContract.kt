package com.mapz.outsource.presentation.activities.main

import com.mapz.outsource.presentation.mvp.IBaseActivity
import com.mapz.outsource.presentation.mvp.IBasePresenter

interface MainContract {
    interface ViewContract: IBaseActivity {

    }

    interface PresenterContract: IBasePresenter<ViewContract> {

    }
}