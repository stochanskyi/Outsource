package com.mapz.outsource.presentation.activities.main.impl

import com.mapz.outsource.presentation.activities.main.MainContract
import com.mapz.outsource.presentation.mvp.BasePresenter

class MainPresenter: BasePresenter<MainContract.ViewContract>(),
    MainContract.PresenterContract{

}