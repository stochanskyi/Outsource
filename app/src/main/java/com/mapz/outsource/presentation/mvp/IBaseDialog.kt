package com.mapz.outsource.presentation.mvp

interface IBaseDialog: IBaseView {
    val dialogTag: String?

    fun close()
}