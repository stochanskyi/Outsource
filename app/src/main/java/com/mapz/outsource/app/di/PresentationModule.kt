package com.mapz.outsource.app.di


import com.mapz.outsource.presentation.activities.main.MainContract
import com.mapz.outsource.presentation.activities.main.impl.MainPresenter
import org.koin.dsl.module

val presentationModule = module {

    //Activities

    factory { MainPresenter() as MainContract.PresenterContract }
    //Fragments

    //Dialogs

}